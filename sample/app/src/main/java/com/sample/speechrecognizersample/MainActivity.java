package com.sample.speechrecognizersample;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getSimpleName();

	private SpeechRecognizer speechRecognizer;
	private State state;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getApplicationContext());
		speechRecognizer.setRecognitionListener(recognitionListener);

		findViewById(R.id.btn_start_stop).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (state == State.Standby) {
					startListening();
					setState(State.Preparing, null);
				} else {
					//speechRecognizer.stopListening();
					speechRecognizer.cancel();
					setState(State.Standby, null);
				}
			}
		});

		setState(State.Standby, null);
	}

	private void startListening() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		speechRecognizer.startListening(intent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		speechRecognizer.cancel();
		speechRecognizer.destroy();
	}

	private enum State {
		Standby(R.string.state_none),
		Preparing(R.string.state_preparing),
		ReadyForSpeech(R.string.state_ready_for_speech),
		Recognizing(R.string.state_recognizing),
		Error(R.string.state_error), ;
		public final int stringResId;

		private State(int stringResId) {
			this.stringResId = stringResId;
		}
	}

	private void setState(State state, String text) {
		this.state = state;

		String stateText = getResources().getString(state.stringResId);
		if (text != null) {
			stateText += "\n" + text;
		}
		((TextView)findViewById(R.id.text_state)).setText(stateText);

		if (state == State.Recognizing) {
			((Button)findViewById(R.id.btn_start_stop)).setText(R.string.btn_stop);
		} else {
			((Button)findViewById(R.id.btn_start_stop)).setText(R.string.btn_start);
		}
	}

	private void setResult(ArrayList<String> results, float[] scores) {
		if (results == null || results.isEmpty()) {
			((TextView)findViewById(R.id.text_state)).setText(R.string.state_error);
			return;
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < results.size(); ++i) {
			sb.append(String.format("(%.3f) %s", scores[i], results.get(i))).append('\n');
		}
		((TextView)findViewById(R.id.text_state)).setText(sb.toString());
	}

	private final RecognitionListener recognitionListener = new RecognitionListener() {
		@Override
		public void onReadyForSpeech(Bundle params) {
			Log.v(TAG, "onReadyForSpeech");
			setState(State.Recognizing, null);
		}

		@Override
		public void onBeginningOfSpeech() {
			Log.v(TAG, "onBeginningOfSpeech");
			setState(State.Recognizing, null);
		}

		@Override
		public void onRmsChanged(float rmsdB) {
		}

		@Override
		public void onBufferReceived(byte[] buffer) {
		}

		@Override
		public void onEndOfSpeech() {
			Log.v(TAG, "onEndOfSpeech");
		}

		@Override
		public void onError(int error) {
			Log.e(TAG, "onError. errorCode=" + error);

			if (error == SpeechRecognizer.ERROR_NO_MATCH ||
				error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {
				startListening();
			} else if (error == SpeechRecognizer.ERROR_CLIENT) {
				speechRecognizer.destroy();
				speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getApplicationContext());
				speechRecognizer.setRecognitionListener(recognitionListener);
			} else {
				setState(State.Error, getErrorMessage(error));
			}
		}

		@Override
		public void onResults(Bundle results) {
			Log.v(TAG, "onResults");
			setState(State.Standby, null);

			ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			float[] scores = results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES);
			setResult(data, scores);

			//speechRecognizer.stopListening();
		}

		@Override
		public void onPartialResults(Bundle partialResults) {
			Log.v(TAG, "onPartialResults");
		}

		@Override
		public void onEvent(int eventType, Bundle params) {
			Log.v(TAG, "onEvent. eventType=" + eventType);
		}
	};

	private String getErrorMessage(int error) {
		switch (error) {
			case SpeechRecognizer.ERROR_NETWORK:
				return "network error";
			case SpeechRecognizer.ERROR_AUDIO:
				return "audio error";
			case SpeechRecognizer.ERROR_CLIENT:
				return "client error";
			case SpeechRecognizer.ERROR_NO_MATCH:
				return "no match";
			case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
				return "network timeout";
			case SpeechRecognizer.ERROR_SERVER:
				return "server error";
			case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
				return "speech timeout";
			case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
				return "recognizer busy";
			default:
				return "unknown error(" + error + ")";
		}
	}
}
